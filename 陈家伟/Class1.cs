﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 九九乘法表
{
    class Class1
    {
        public static void Main(string[] args)
        {
            int i;
            int j;
            for (i = 1; i <= 9; i++)
            {
                for (j = 1; j <= i; j++)
                {
                    Console.Write("{0}*{1}={2}\t", i, j, i * j);//要对齐
                }
                Console.WriteLine();//要换行
            }
        }
    }
}