﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace work3
{
    class Program
    {
        static void Main(string[] args)
        {
            //九九乘法表
            for (int i = 1; i < 10; i++)
            {
                for (int h = 1; h <= i; h++)
                {
                    Console.Write("{0} X {1} = {2}\t", h, i, i * h);
                }
                Console.WriteLine();
            }

            //直角三角形
            Console.WriteLine("请输入行数：");
            int num = int.Parse(Console.ReadLine());
            for (int i = 1; i <= num; i++)
            {
                for (int h = 1; h <= i; h++)
                {
                    Console.Write("*");
                }
                Console.WriteLine();
            }

            //倒直角三角形
            Console.WriteLine("请输入行数：");
            int num1 = int.Parse(Console.ReadLine());
            for (int i = 1; i <= num1; i++)
            {
                for (int n = 1; n <= num1 + 1 - i; n++)
                {
                    Console.Write("*");
                }
                Console.WriteLine();
            }
        }
    }
}
