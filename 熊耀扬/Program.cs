﻿using System;

namespace Demo03
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 1; i < 10;i++)
            {
                for (int j = 1;j <= i; j++)
                {
                    Console.Write(i + "x" + j + "=" + i * j + "\t");
                }
                Console.WriteLine("");
            }


            int x = 15 / 2;
            for (int a = -1*x;a <= x;a++)
            {
                for (int b = 1;b <=Math.Abs(a);b++ )
                {
                    Console.Write(" ");
                }
                for (int b = 1;b <= 15-2*Math.Abs(a);b++)
                {
                    Console.Write("*");
                }
                Console.WriteLine();
            }
        }
    }
}
